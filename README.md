# Computer Setup Notes

I wiped my hard drive for reasons and am starting setup with a fresh macOS install with nothing else installed so it's all nice and fresh. 😬

# Table of Contents

- [1password for Mac](#1password-for-mac)
- [Migration of Files, etc.](#migration-of-files-etc)
    - [iCloud Drive](#icloud-drive)
    - [Google Drive](#google-drive)
    - [Time Machine Backup](#time-machine-backup)
    - [1password](#1password)
- [macOS Setup](#macos-setup)
    - [System Settings](#system-settings)
    - [Finder](#finder)
    - [Git](#git)
    - [Misc.](#misc)
- [Jetbrains](#jetbrains)
    - [Toolbox App](#toolbox-app)
    - [IDE Settings](#ide-settings)
        - [IntelliJ](#intellij)
        - [PyCharm](#pycharm)
        - [WebStorm](#webstorm)
        - [Android Studio](#android-studio)
        - [DataGrip](#datagrip)
        - [Writerside](#writerside)
- [Browsers](#browsers)
    - [Chrome](#chrome)
    - [Chrome Canary](#chrome-canary)
    - [Edge](#edge)
    - [Extensions on Both Chrome Browsers](#extensions-on-both-chrome-browsers)
    - [Extensions on all Three Browsers](#extensions-on-all-three-browsers)
- [Additional Apps](#additional-apps)

## [1password for Mac](https://1password.com/downloads/mac/)

- I installed this first because it is so much easier to have all my passwords and notes easily accessible while installing and setting up all the other apps. :)
- In the settings make sure it is set to 'stay in the menu bar' and 'start at login'

## Migration of Files, etc.

### iCloud Drive

- should show up automatically so just need to download files locally

### [Google Drive](https://www.google.com/drive/download/)

- login with invisiblepinkunicorn account
- drag 'My Drive' to Finder sidebar
- download everything in 'My Drive' to restore files locally

### Time Machine Backup
    
- copy `~/workspace` to home directory and then drag to Finder sidebar
- copy `~/.git-prompt.sh` to home directory
- most other things should be backed up on Google Drive or iCloud, but check any other folders (`~/Documents`, etc) for things that might want to be copied over

### 1password
   
- SSH keys are saved in a secure note. Make `~./ssh` directory and create `id_rsa` and `id_rsa.pub` with the saved keys

## macOS Setup

### System Settings

- Notifications ->
    - turn off except for Find My
- General ->
    - Software Update ->
        - turn on 'Install macOS updates'
- Control Center ->
    - change 'Automatically hide and show the menu bar' to 'Never'
- Desktop & Dock ->
    - turn on 'Automatically hide and show the Dock'
    - turn off 'Animate opening applications'
    - turn off 'Show suggested and recentapps in Dock'
- Battery ->
    - set 'Low Power Mode' to 'Only on Battery'
- Lock Screen ->
    - set 'Turn display off on bettery when inactive' to 'For 10 minutes'
    - set 'Turn display off on power adapter when inactive' to 'For 1 hour'
- Touch ID & Password ->
    - add Fingerprints
- Internet Accounts -> 
    - iCloud ->
        - turn off 'iCloud Mail' 
        - Show More Apps ->
            - uncheck iCloud Calendar, Reminders, Safari, Stocks, Home
- Keyboard ->
    - turn off 'Keyboard brightness'
    - Touch Bar Settings ->
        - set 'Touch Bar shows' to 'Expanded Control Strip'
        - Customize Control Strip ->
            - replace 'Mission Control' and 'Launchpad' with 'Screenshot' and 'Done'
- Trackpad ->
    - Point & Click ->
        - change 'Look up & data detectors' to 'Tap with Three Fingers'
        - turn on 'Tap to click'
    - More Gestures ->
        - set 'Mission Control' to 'Swipe Up with Four Fingers'
        - turn off 'Launchpad'
- set Appearance, Wallpaper, and Screen Saver as desired

### Finder

- Settings ->
    - General ->
        - uncheck everything showing on the desktop
        - change 'New Finder windows show' to 'downloads'
    - Sidebar ->
        - check 'ktown'
        - uncheck 'Recents', 'Airdrop', 'Desktop', and 'Documents' (all documents should be saved on Google Drive)
    - Advanced ->
        - check 'Show all filename extentions'
        - check 'Remove items from th erash after 30 days'
        - check both oprions in 'Keep folders on top'
- set to display 'as Columns'
- set to order by 'Kind'
- order Favorites in sidebar: ktown, Applications, My Drive, workspace, Downloads

### Git

- make `~/.zshrc` and add
    ```
    # aliases
    alias gst='git status'

    # git prompt
    GIT_PS1_SHOWCOLORHINTS=true
    GIT_PS1_SHOWDIRTYSTATE=true
    GIT_PS1_SHOWUNTRACKEDFILES=true
    GIT_PS1_SHOWUPSTREAM="auto verbose name git"
    source ~/.git-prompt.sh

    # shows branch name
    precmd () { __git_ps1 "%1~" "$ " " (%s)" }
    ```
- make `~/.gitconfig` and add
    ```
    [user]
        name = Kristin Brooks
        email = kristin.ruth.brooks@gmail.com
    [alias]
        gst = status
        ci = commit
        co = checkout
        sw = switch
    [credential]
        helper = osxkeychain
    [core]
        untrackedCache = true
        editor = vim
    [push]
        default = current
    [pull]
        rebase = true
    [feature]
        manyFiles = true
    [init]
        defaultBranch = main

    ```
- additional info/options: [Git tips and tricks](https://about.gitlab.com/blog/2016/12/08/git-tips-and-tricks/#see-the-repository-status-in-your-terminals-prompt)

### Misc.

- set nopasswd sudoers 
    - run `sudo visudo`
    - change "`%admin ...`" line to read: `%admin   ALL=(ALL) NOPASSWD:ALL`
- install Xcode from AppStore
- install Command Line Tools: `xcode-select --install`
- connect Bluetooth Devices
- setup printer/scanner

## Jetbrains

### [Toolbox App](https://www.jetbrains.com/toolbox-app/)

- settings ->
    - set to dark theme
    - set download folder for tools to '/Applications'
- install all IDEs you use

### IDE Settings

- set all to show function keys

#### IntelliJ

#### PyCharm

#### WebStorm

#### Android Studio

#### DataGrip

#### Writerside

Testing this out to see if I like it for documentation.

#### RubyMine

#### Aqua

Still in preview, but want to try it out.

## Browsers

### [Chrome](https://www.google.com/chrome/)

- for work use, log in as kristin.ruth.brooks
- Extensions only on Chrome:
    - [React Developer Tools](https://chromewebstore.google.com/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)
    - [Vue.js devtools](https://chromewebstore.google.com/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd)
    - [Pesticide for Chrome](https://chromewebstore.google.com/detail/pesticide-for-chrome/bakpbgckdnepkmkeaiomhmfcnejndkbi)
    - [JSON Viewer](https://chromewebstore.google.com/detail/json-viewer/gbmdgpbipfallnflgajpaliibnhdgobh)
    - [CSS Peeper](https://chromewebstore.google.com/detail/css-peeper/mbnbehikldjhnfehhnaidhjhoofhpehk)

### [Chrome Canary](https://www.google.com/chrome/canary/)

- for personal use, log in as invisiblepinkunicorn
- Extensions only on Canary:
    - [Language Reactor](https://chromewebstore.google.com/detail/language-reactor/hoombieeljmmljlkjmnheibnpciblicm)
    - [Save to Pinterest](https://chromewebstore.google.com/detail/save-to-pinterest/gpdjojdkbbmdfjfahjcgigfpmkopogic)
    - [Merlin](https://chromewebstore.google.com/detail/merlin-1-click-access-to/camppjleccjaphfdbohjdohecfnoikec)

### [Edge](https://www.microsoft.com/en-us/edge/download?form=MA13FJ)

- use for Copilot search

### Extensions on Both Chrome Browsers

- [Google Docs Offline](https://chromewebstore.google.com/detail/google-docs-offline/ghbmnnjooekpmoecnnnilnnbdlolhkhi)
- [Deluminate](https://chromewebstore.google.com/detail/deluminate/iebboopaeangfpceklajfohhbpkkfiaa)
- [Zoom](https://chromewebstore.google.com/detail/zoom-chrome-extension/kgjfgplpablkjnlkjmjdecgdpfankdle?hl=en&gl=US&authuser=1&pli=1)
- [Site Palette](https://chromewebstore.google.com/detail/site-palette/pekhihjiehdafocefoimckjpbkegknoh)
- [Fonts Ninja](https://chromewebstore.google.com/detail/fonts-ninja/eljapbgkmlngdpckoiiibecpemleclhh)
- [ColorZilla](https://chromewebstore.google.com/detail/colorzilla/bhlhnicpbhignbdhedgjhgdocnmhomnp)
- [Workona Tab Groups Helper](https://chromewebstore.google.com/detail/tab-groups-helper/ggfkdnnfhlmhbeenlgcbbjgpekdmjpig)

### Extensions on all Three Browsers

- Workona [Chrome](https://chromewebstore.google.com/detail/tab-manager-by-workona/ailcmbgekjpnablpdkmaaccecekgdhlh)/[Edge](https://microsoftedge.microsoft.com/addons/detail/workona-tab-manager/gdfnelpciiajgjenlapgkdcjpcfpfpob)
- 1password [Chrome](https://chromewebstore.google.com/detail/1password-%E2%80%93-password-mana/aeblfdkhhhdcdjpifhhbdiojplfjncoa)/[Edge](https://microsoftedge.microsoft.com/addons/detail/1password-%E2%80%93-password-mana/dppgmdbiimibapkepcbdbmkaabgiofem)
- Choosy [Chrome](https://chromewebstore.google.com/detail/choosy/baecahhpgcpccohoeipmdkkbemhjhfmc)/[Edge](https://microsoftedge.microsoft.com/addons/detail/choosy/igacgeaoedkpiodoehejdpacinodeeei)
- Adblock Plus [Chrome](https://chromewebstore.google.com/detail/adblock-plus-free-ad-bloc/cfhdojbkjhnklbpkdaibdccddilifddb)/[Edge](https://microsoftedge.microsoft.com/addons/detail/adblock-plus-free-ad-bl/gmgoamodcdcjnbaobigkjelfplakmdhh)

## Additional Apps

- [Timing](https://timingapp.com/) - automatic time tracking by app usage
    - set application rules for Communication, Personal, Productivity, and Studying
- Clocker (from AppStore) - date/time zone app
    - add cities you care about the time difference with
- [Rectangle](https://rectangleapp.com/) - shortcuts for moving and resizing windows
    - give Accessibilty permissions in System Settings
    - Settings ->
        - (Rectangle with Corner X) ->
            - remove all defaults
            - set 'Maximize' to 'Ctrl+Option+Cmd+m'
            - set 'Next Display' to 'Ctrl+Option+Cmd+n'
            - set 'Left Half'/'Right Half'/'Top Half'/'BottomHalf' to 'Ctrl+Option+Cmd+left/right/up/down'
            - set 'Top Left' to 'Ctrl+Option+Cmd+Shift+left'
            - set 'Top Right' to 'Ctrl+Option+Cmd+Shift+up'
            - set 'Bottom Left' to 'Ctrl+Option+Cmd+Shift+down'
            - set 'Bottom Right' to 'Ctrl+Option+Cmd+Shift+right'
        - (Gear Icon) ->
            - check 'Launch at login'
            - check 'Hide menu bar icon'
            - check 'Check for updates automatically'
            - change 'Repeated commands' to 'cycle 1/2, 2/3, and 1/3 on half actions'
- [Clipy](https://clipy-app.com/) - clipboard extension app
    - during install check launch on system startup
    - Preferences ->
        - General ->
            - set 'Max clipboard history size' to 2000
        - Menu ->
            - set 'Number of items place inline' to 30 items
            - set 'Number of items place inside a folder' to 30 items
            - set 'Number of characters in the menu' to 200 chars
        - Shortcuts ->
            - change 'Main' to Ctrl+Option+Cmd+V
            - disable other shortcuts to avoid conflicts with Jetbrains
- [iTerm2](https://iterm2.com/) - terminal replacement
    - Settings ->
        - General ->
            - Selection ->
                - remove `\` from 'Characters considered part of a word'
        - Appearance ->
            - Tabs ->
                - check 'Show tab bar even when there is only one tab'
            - Dimming ->
                - check 'Dim background windows'
        - Profiles ->
            - General ->
                - under 'Working Directory' check 'Reuse previous session's directory'
            - Terminal ->
                - under 'Scrollback Buffer' check 'Unlimited scrollback'
            - Keys ->
                - set 'Left Option key' to 'Esc+'
- [Warp](https://www.warp.dev/a) - terminal replacement
- [Textmate](https://macromates.com/) - text editor
    - Settings ->
        - Files ->
            - uncheck 'Open documents from last session'
        - Projects ->
            - change 'Include files matching' to `{*,.*}`
        - Terminal ->
            - Install Shell Support
    - to `~/.zshrc` add 
    ```
    # mate command for textmate
    export EDITOR="/usr/local/bin/mate -w"
    ```
    - Configure autosave: add `saveOnBlur = true` to top of 
    `~/Library/Application\ Support/TextMate/Global.tmProperties`
- [Zed](https://zed.dev/) - collaborative editor
    - set keymap to Jetbrains
- Slack (from AppStore)
    - set theme to Dark
- [Zoom](https://us04web.zoom.us/download#client_4meeting)
    - Settings ->
        - General ->
            - check 'Use dual monitors'
            - check 'Enter full screen when starting or joining a meeting'
            - uncheck 'Add Zoom to macOS menu bar'
        - Video ->
            - check 'Touch up my appearance'
            - check 'Adjust for low light'
            - check 'Always display participant name on their videos'
        - Audio ->
            - check 'Mute my mic when joining'
        - Background & Effects ->
            - change Virtual Backgrounds to 'Blur'
- [Discord](https://discord.com/download)
- [Choosy](https://choosy.app/) - opens links in the right browser
    - Preferences ->
        - Browsers ->
            - make Choosy the default browser
            - add browsers to prioritization list
        - Advanced ->
            - click 'Start Choosy at login'
- [Contexts](https://contexts.co/) - window switcher
    - Sidebar ->
        - change 'Show sidebar on' to 'No display'
        - uncheck 'Auto adjust windows widths so they are not ovelapped by Sidebar'
    - Panel ->
        - change 'Show Panel on' to 'Active Display'
    - Command-Tab ->
        - Check 'Option-Tab' and 'Option-Backtick(`)' on the list and change 'Show windows of' to 'Active App' for each
        - check 'Typing characters starts Fast Search when Panel is visible'
- Kindle (from AppStore)
- [Homebrew](https://brew.sh/) - package manager
    - to install run `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"` 
- [Obsidian](https://obsidian.md/)
    - TODO: setup
